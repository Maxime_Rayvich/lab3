/* Maxime Rayvich, 2032446
*/

package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Vector;

import org.junit.Test;

public class Vector3dTests {


    @Test
    // this test will succeed. In practice we would put
    // a real method call into the assertEquals
    public void test2() {
        assertEquals(1,1);
    }

    @Test
    // this will test all get methods from Vector3d.java
    public void testGets(){
        Vector3d vector = new Vector3d(3, 8, 9);
        assertEquals(vector.getX(), 3);
        assertEquals(vector.getY(), 8);
        assertEquals(vector.getZ(), 9);
    }

    @Test
    // this will test the magnitude method  
    public void testMagnitude(){
        Vector3d vector = new Vector3d(4, 2, 4);
        assertEquals(vector.magnitude(), 6);

    }
    @Test
    // this will test the dotProduct method
    public void testDotProduct(){
    Vector3d v1 = new Vector3d(1, 2, 3);
    Vector3d v2 = new Vector3d(3, 2, 1);
    

    // expected result: 10
    double Result = v1.dotProduct(v2);
    assertEquals(Result, 10);
    }



    @Test
    // this will test the add method
    public void testAdd(){
        Vector3d v1 = new Vector3d(1, 2, 3);
        Vector3d v2 = new Vector3d(3,2,1);
        Vector3d vFinal = v1.add(v2);

        // should equal to 4,4,4
        Vector3d vTest = new Vector3d(4, 4, 4);

        assertEquals(vFinal.getX(), vTest.getX());
        assertEquals(vFinal.getY(), vTest.getY());
        assertEquals(vFinal.getZ(), vTest.getZ());


    }


}
