/* Maxime Rayvich, 2032446
*/
package LinearAlgebra;

public class Vector3d{
    private double x;
    private double y;
    private double z;


    public Vector3d(double ex, double why, double zed){
        this.x = ex;
        this.y = why;
        this.z = zed;
    }
    public double getX(){
        return this.x;
    }
    public double getY(){
        return this.y;
    }
    public double getZ(){
        return this.z;
    }
    public double magnitude(){
        double magnitude = Math.sqrt(this.x*this.x + this.y*this.y+this.z*this.z);
        return magnitude;
    }
    public double dotProduct(Vector3d vect){
        return this.x * vect.x + this.y * vect.y + this.z * vect.z;
    }

    public Vector3d add(Vector3d vect){
        Vector3d addedVector = new Vector3d(this.x + vect.x,this.y + vect.y ,this.z + vect.z );
        return addedVector;
    }


}